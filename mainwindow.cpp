#include "mainwindow.h"
#include "chessboard.h"
#include "chessview.h"
#include "chessalgorithm.h"
#include <QLayout>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),  m_selectedField(0)
{


    resize(528, 481);
    centralwidget = new QWidget(this);
    centralwidget->setObjectName(QStringLiteral("centralwidget"));
    horizontalLayout = new QHBoxLayout(centralwidget);
    horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
    verticalLayout = new QVBoxLayout();
    verticalLayout->setObjectName(QStringLiteral("verticalLayout"));

    addPixelButton = new QRadioButton(centralwidget);
    addPixelButton->setObjectName(QStringLiteral("addPixelButton"));
    addPixelButton->setChecked(false);
    removePixelButton = new QRadioButton(centralwidget);
    removePixelButton->setObjectName(QStringLiteral("removePixelButton"));
    removePixelButton->setChecked(true);

    myButtonGroup = new QButtonGroup(this);
    myButtonGroup->addButton(removePixelButton);
    myButtonGroup->addButton(addPixelButton);


    verticalLayout->addWidget(removePixelButton);
    verticalLayout->addWidget(addPixelButton);

    verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

    verticalLayout->addItem(verticalSpacer);


    horizontalLayout->addLayout(verticalLayout);

    chessViewWidget = new ChessView(centralwidget);
    chessViewWidget->setObjectName(QStringLiteral("chessViewWidget"));
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(1);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(chessViewWidget->sizePolicy().hasHeightForWidth());
    chessViewWidget->setSizePolicy(sizePolicy);

    horizontalLayout->addWidget(chessViewWidget);

    setCentralWidget(centralwidget);

    setWindowTitle("AI Chess");
    addPixelButton->setText("Add Pixel");
    removePixelButton->setText("Remove Pixel");

    connect(addPixelButton, SIGNAL(clicked()), this, SLOT(on_addPixelButton_clicked()));
    connect(removePixelButton, SIGNAL(clicked()), this, SLOT(on_removePixelButton_clicked()));

    actionSave_pixels = new QAction(this);
    actionSave_pixels->setObjectName("actionSave_pixels");
    QIcon icon;
    icon.addFile(":/icons/upload.png", QSize(), QIcon::Normal, QIcon::On);
    actionSave_pixels->setIcon(icon);

    actionDownload_pixels = new QAction(this);
    actionDownload_pixels->setObjectName("actionDownload_pixels");
    QIcon icon1;
    icon1.addFile(":/icons/download.png", QSize(), QIcon::Normal, QIcon::On);
    actionDownload_pixels->setIcon(icon1);

    actionReset_pixels = new QAction(this);
    actionReset_pixels->setObjectName("actionReset_pixels");
    QIcon icon3;
    icon3.addFile(":/icons/refresh.png", QSize(), QIcon::Normal, QIcon::On);
    actionReset_pixels->setIcon(icon3);

    actionSet_pixels = new QAction(this);
    actionSet_pixels->setObjectName("actionSet_pixels");
    QIcon icon4;
    icon4.addFile(":/icons/th-small-outline.png", QSize(), QIcon::Normal, QIcon::On);
    actionSet_pixels->setIcon(icon4);

    actionCalculate = new QAction(this);
    actionCalculate->setObjectName("actionCalculate");
    QIcon icon5;
    icon5.addFile(":/icons/calculator.png", QSize(), QIcon::Normal, QIcon::On);
    actionCalculate->setIcon(icon5);

    menuBar = new QMenuBar(this);
    menuBar->setObjectName("menuBar");
    menuBar->setGeometry(QRect(5, 0, 395, 45));
    menuMenu = new QMenu(menuBar);
    menuMenu->setObjectName("menuMenu");
    QIcon icon2;
    icon2.addFile(":/icons/th-menu.png", QSize(), QIcon::Normal, QIcon::On);
    menuMenu->setIcon(icon2);
    setMenuBar(menuBar);
    mainToolBar = new QToolBar(this);
    mainToolBar->setObjectName("mainToolBar");
    addToolBar(Qt::TopToolBarArea, mainToolBar);
    statusBar = new QStatusBar(this);
    statusBar->setObjectName("statusBar");
    setStatusBar(statusBar);

    menuBar->addAction(menuMenu->menuAction());
    menuMenu->addAction(actionSave_pixels);
    menuMenu->addAction(actionDownload_pixels);
    menuMenu->addAction(actionReset_pixels);
    menuMenu->addAction(actionSet_pixels);
    menuMenu->addAction(actionCalculate);
    mainToolBar->addAction(actionSave_pixels);
    mainToolBar->addAction(actionDownload_pixels);
    mainToolBar->addAction(actionReset_pixels);
    mainToolBar->addAction(actionSet_pixels);
    mainToolBar->addSeparator();
    mainToolBar->addAction(actionCalculate);

    actionSave_pixels->setText("Save pixels");
    actionDownload_pixels->setText("Download pixels");
    actionReset_pixels->setText("Reset pixels");
    actionSet_pixels->setText("Set size of board");
    actionCalculate->setText("Calculate probability");
    menuMenu->setTitle("Menu");

    QMetaObject::connectSlotsByName(this);

    m_view = new ChessView(chessViewWidget);
    connect(m_view, SIGNAL(clicked(QPoint)), this, SLOT(viewClicked(QPoint)));

    m_view->setPiece('P', QIcon(":/pieces/Chess_plt45.svg")); // pawn
    m_view->setPiece('K', QIcon(":/pieces/Chess_klt45.svg")); // king
    m_view->setPiece('Q', QIcon(":/pieces/Chess_qlt45.svg")); // queen
    m_view->setPiece('R', QIcon(":/pieces/Chess_rlt45.svg")); // rook
    m_view->setPiece('N', QIcon(":/pieces/Chess_nlt45.svg")); // knight
    m_view->setPiece('B', QIcon(":/pieces/Chess_blt45.svg")); // bishop

    m_view->setPiece('p', QIcon(":/pieces/Chess_pdt45.svg")); // pawn
    m_view->setPiece('k', QIcon(":/pieces/Chess_kdt45.svg")); // king
    m_view->setPiece('q', QIcon(":/pieces/Chess_qdt45.svg")); // queen
    m_view->setPiece('r', QIcon(":/pieces/Chess_rdt45.svg")); // rook
    m_view->setPiece('n', QIcon(":/pieces/Chess_ndt45.svg")); // knight
    m_view->setPiece('b', QIcon(":/pieces/Chess_bdt45.svg")); // bishop

    m_algorithm = new ChessAlgorithm(this);
    m_algorithm->newGame();
    m_view->setBoard(m_algorithm->board());
   // m_view->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_view->setFieldSize(QSize(50,50));


    layout()->setSizeConstraint(QLayout::SetFixedSize);

    if (!QFile::exists("countFilesSaved.txt"))
    {
        statusBar->showMessage("No files saved", 5000);
        countFilesSaved = 0;
    }
    else
    {
        QFile file("countFilesSaved.txt");
        if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
        {
            statusBar->showMessage("No files saved", 1500);
            countFilesSaved = 0;
        }

        else
        {
            QString str = file.readAll();
            countFilesSaved = str.toInt();
            statusBar->showMessage(str + " file(s) saved", 5000);
            file.close();
        }
    }
    qDebug()<<countFilesSaved;

    currentFileOpened = QString();

    chessViewWidget->setMinimumSize(QSize(m_view->fieldSize().width()*m_view->board()->columns()+50,
                                          m_view->fieldSize().height()*m_view->board()->ranks()+50));
    //chessViewWidget->setFocusPolicy(Qt::StrongFocus);
}

MainWindow::~MainWindow()
{
    if (m_view){delete m_view; }
    if (m_algorithm) {delete m_algorithm;}
    if (m_selectedField) {delete m_selectedField;}

    if (addPixelButton) {delete addPixelButton; }
    if (removePixelButton) {delete removePixelButton; }
    if (myButtonGroup) {delete myButtonGroup;}

    if (actionReset_pixels) {delete actionReset_pixels;}
    if (actionDownload_pixels) {delete actionDownload_pixels; }
    if (actionSave_pixels) {delete actionSave_pixels; }

    if (statusBar) {delete statusBar; }
    if (mainToolBar) {delete mainToolBar; }
    if (menuMenu) {delete menuMenu; }
    if (menuBar) {delete menuBar;}

    if (verticalSpacer)
    {
        verticalLayout->removeItem(verticalSpacer);
        delete verticalSpacer;
    }

    if (chessViewWidget) {delete chessViewWidget; }

    if (verticalLayout) {delete verticalLayout;}
    if (horizontalLayout) {delete horizontalLayout; }
    if (centralwidget) {delete centralwidget; }


}

void MainWindow::viewClicked(const QPoint &field)
{
//    if(m_clickPoint.isNull()) {
//        if(m_view->board()->data(field.x(), field.y()) != ' ') {
//            m_clickPoint = field;
//            m_selectedField = new ChessView::FieldHighlight(
//                        field.x(), field.y(), QColor(255, 0, 0, 50)
//                        );
//            m_view->addHighlight(m_selectedField);
//        }
//    } else {
//        if(field != m_clickPoint) {
//            m_view->board()->movePiece(m_clickPoint.x(), m_clickPoint.y(), field.x(), field.y());
//        }
//        m_clickPoint = QPoint();
//        m_view->removeHighlight(m_selectedField);
//        delete m_selectedField;
//        m_selectedField = 0;
    m_view->board()->setDataInternal(field.x(), field.y(), m_view->board()->getCurrentChar());
    repaint();
}

void MainWindow::on_addPixelButton_clicked()
{
    m_view->board()->setCurrentChar('P');
}

void MainWindow::on_removePixelButton_clicked()
{
    m_view->board()->setCurrentChar(' ');
}

void MainWindow::on_actionSave_pixels_triggered(bool)
{
    QMessageBox messageBox;

    messageBox.setWindowTitle("Save pixels");
    messageBox.setWindowIcon(QIcon(":/icons/upload.png"));
    messageBox.setIconPixmap(QPixmap(":/icons/upload.png"));
    QPushButton* newFileButton = messageBox.addButton("New file", QMessageBox::YesRole);
    QPushButton* currentFileButton = messageBox.addButton("Current file", QMessageBox::NoRole);
    QPushButton* cancelButton = messageBox.addButton("Cancel", QMessageBox::NoRole);

    if(currentFileOpened.isEmpty())
    {
        currentFileButton->setDisabled(true);
        messageBox.setText("No file is opened\nYou can save the pixels to a new file");
    }

    else
    {
        messageBox.setText("Where do you want to save it?\n\n"+currentFileOpened+" is open now");
    }

    messageBox.exec();

    if (messageBox.clickedButton() == newFileButton)
    {
        countFilesSaved++;
        QString tmp;
        QString newFileName("myboard_"+tmp.setNum(countFilesSaved)+".txt");
        qDebug()<<newFileName;

        QString str ="";
        for (int i = 0; i<m_view->board()->boardData().size(); i++)
        {
            str+=m_view->board()->boardData()[i];
        }
        qDebug()<<"str to save = "<<str;

        QFile file(newFileName);
        QTextStream stream(&file);

        if (!file.open(QIODevice::WriteOnly|QIODevice::Text))
        {
             statusBar->showMessage("Saving failed", 1500);
             return;
        }

        stream << str;
        stream.flush();
        file.close();

        statusBar->showMessage("Board saved to file " + newFileName, 1500);
        currentFileOpened = newFileName;

        QFile file1("countFilesSaved.txt");
        QTextStream stream1(&file1);

        if (!file1.open(QIODevice::WriteOnly|QIODevice::Text))
        {
             statusBar->showMessage("Saving number of files failed", 5000);
             return;
        }

        str = QString();
        str.setNum(countFilesSaved);
        qDebug()<<str;
        stream1 << str;
        stream1.flush();
        file1.close();

    }

    else if (messageBox.clickedButton() ==  currentFileButton)
    {
        QString str ="";
        for (int i = 0; i<m_view->board()->boardData().size(); i++)
        {
            str+=m_view->board()->boardData()[i];
        }

        qDebug()<<"str to save = "<<str;
        QFile file(currentFileOpened);
        QTextStream stream(&file);

        if (!file.open(QIODevice::WriteOnly|QIODevice::Text))
        {
             statusBar->showMessage("Saving failed", 1500);
             return;
        }

        stream << str;
        stream.flush();
        file.close();

        statusBar->showMessage("Board saved to file " + currentFileOpened, 1500);
    }

    else {}

    qDebug()<<countFilesSaved;
}

void MainWindow::on_actionDownload_pixels_triggered(bool)
{
    if (!countFilesSaved)
    {
        statusBar->showMessage("No file saved", 1500);
        return;
    }

    currentFileOpened = QFileDialog::getOpenFileName(0, "Download pixels", ".", "myboard*.txt");

    if (!QFile::exists(currentFileOpened))
    {
        statusBar->showMessage("No file", 1500);
        return;
    }

    QFile file(currentFileOpened);
    if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
    {
         statusBar->showMessage("Downloading failed", 1500);
         return;
    }

    QString str = file.readAll();
    file.close();

    qDebug()<<"str to download = "<<str;

    QString::iterator it = str.begin();

    for (int rank = 1; rank<=m_view->board()->ranks(); rank++)
    {
        for (int col = 1; col<=m_view->board()->columns(); col++)
        {
            if (it!=str.end())
            {
                m_view->board()->setDataInternal(col, rank, (*it).toLatin1());
                ++it;
            }
            else
            {
                  m_view->board()->setDataInternal(col, rank, ' ');
            }
        }
    }
    repaint();

    statusBar->showMessage("Board downloaded", 1500);
}

void MainWindow::on_actionReset_pixels_triggered(bool)
{
    QMessageBox messageBox;
    messageBox.setText("Are you sure?");
    messageBox.setWindowTitle("Reset pixels");
    messageBox.setWindowIcon(QIcon(":/icons/refresh.png"));
    messageBox.setIconPixmap(QPixmap(":/icons/refresh.png"));
    QPushButton* yesButton = messageBox.addButton(QMessageBox::Yes);
    QPushButton* noButton = messageBox.addButton(QMessageBox::No);
    messageBox.exec();
    if(messageBox.clickedButton()==yesButton)
    {
        qDebug()<<"Yes";

        for (int rank = 1; rank<=m_view->board()->ranks(); rank++)
        {
            for (int col = 1; col<=m_view->board()->columns(); col++)
            {
                m_view->board()->setDataInternal(col, rank, ' ');
            }
        }
        repaint();

        statusBar->showMessage("Pixels reset", 1500);
    }
}

void MainWindow::on_actionSet_pixels_triggered(bool)
{
qDebug()<<"Set pixels works";

    int newRanks = QInputDialog::getInt(this, "Ranks", "Set number of ranks: ", m_view->board()->ranks(), 1, 30);
    int newColumns = QInputDialog::getInt(this, "Columns", "Set number of columns: ", m_view->board()->columns(), 1, 30);
    m_view->board()->setRanks(newRanks);
    m_view->board()->setColumns(newColumns);
    chessViewWidget->setMinimumSize(QSize(m_view->fieldSize().width()*m_view->board()->columns()+50,
                                          m_view->fieldSize().height()*m_view->board()->ranks()+50));
    m_view->resize(m_view->sizeHint());
    m_view->repaint();
    chessViewWidget->repaint();
    repaint();

    addPixelButton->setChecked(true);
    m_view->board()->setCurrentChar('P');
}

void MainWindow::on_actionCalculate_triggered(bool)
{
    qDebug()<<"Calculate slot works";

    if (!countFilesSaved)
    {
        statusBar->showMessage("No file saved", 1500);
        return;
    }

    //memorize the current situation on board
    QString currentString ="";
    for (int i = 0; i<m_view->board()->boardData().size(); i++)
    {
        currentString+=m_view->board()->boardData()[i];
    }

    //creating list of samples
    QList<QString> myList;

    //filling it (until we say it's enough)
    while (true)
    {
        QString fileOpened = QFileDialog::getOpenFileName(0, "Download pixels", ".", "myboard*.txt");

        if (fileOpened.isEmpty()) //no file selected
        {
            break;
        }

        if (!QFile::exists(fileOpened))
        {
            statusBar->showMessage("No file", 1500);
            continue;
        }

        QFile file(fileOpened);
        if (!file.open(QIODevice::ReadOnly|QIODevice::Text))
        {
            statusBar->showMessage("Downloading failed", 1500);
            continue;
        }

        QString tmpString = file.readAll();
        file.close();

        if (tmpString.isEmpty() == false)
        {
            myList.push_back(tmpString);
        }

        if (QMessageBox::question(this, "Add more files", "Do you want to add more files?", QMessageBox::Yes|QMessageBox::No)
                == QMessageBox::No)
        {
            break;
        }
    }

    //calculating
    double probability = ChessAlgorithm::computeProbability(myList, currentString);

    if (probability==-1) //returns if different length of strings
    {
        QMessageBox::warning(this, "Probability is not calculated", "Different length of strings!", QMessageBox::Ok);
        return;
    }

    if (probability==-2) //returnes if nothing to compare
    {
        QMessageBox::warning(this, "Probability is not calculated", "Nothing to compare!", QMessageBox::Ok);
        return;
    }
    QString probabilityString;
    probabilityString.setNum(probability*100);  //percentage

    QMessageBox::information(this, "Probability calculated", "Probability is "+probabilityString+"%", QMessageBox::Ok);
}

