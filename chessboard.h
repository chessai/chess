#ifndef CHESSBOARD_H
#define CHESSBOARD_H

#include <QObject>
#include <QVector>


class ChessBoard : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int ranks READ ranks NOTIFY ranksChanged)
    Q_PROPERTY(int columns READ columns NOTIFY columnsChanged)
public:
    explicit ChessBoard(int ranks = 8, int columns = 8, QObject *parent = 0);

    int ranks() const { return m_ranks; }
    int columns() const { return m_columns; }
    char data(int column, int rank) const;
    void setData(int column, int rank, char value);
    void movePiece(int fromColumn, int fromRank, int toColumn, int toRank);
    void setFen(const QString &fen);
    void addCharToVector(const char symbol) {m_boardData.push_back(symbol);}
    void popBackFromVector() {m_boardData.pop_back();}

    QVector<char> boardData() const;
    void setBoardData(const QVector<char> &boardData);

    char getCurrentChar() const;
    void setCurrentChar(char value);
    bool setDataInternal(int column, int rank, char value);

    void setRanks(int newRanks);
    void setColumns(int newColumns);

protected:

    void initBoard();


signals:
    void ranksChanged(int);
    void columnsChanged(int);
    void boardReset();
    void dataChanged(int c, int r);

private:
    int m_ranks, m_columns;
    QVector<char> m_boardData;
    char currentChar;
};

#endif // CHESSBOARD_H
