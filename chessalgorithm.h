#ifndef CHESSALGORITHM_H
#define CHESSALGORITHM_H

#include <QObject>//comc
#include <QStringList>
#include <QString>
#include <QVector>
#include <QDebug>
#include <QChar>

class ChessBoard;

class ChessAlgorithm : public QObject
{
    Q_OBJECT
public:
    explicit ChessAlgorithm(QObject *parent = 0);
    ~ChessAlgorithm();
    ChessBoard* board() const;

    static double computeProbability(QList<QString> myStrings, QString stringToCompare);

public slots:
    virtual void newGame();

signals:
    void boardChanged(ChessBoard*);

protected:
    virtual void setupBoard();
    void setBoard(ChessBoard *board);

private:
    ChessBoard* m_board;
    

};

#endif // CHESSALGORITHM_H
