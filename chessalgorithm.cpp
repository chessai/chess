#include "chessalgorithm.h"
#include "chessboard.h"

ChessAlgorithm::ChessAlgorithm(QObject *parent) : QObject(parent), m_board(0)
{

}

ChessAlgorithm::~ChessAlgorithm()
{
    if (m_board)
        delete m_board;
}

ChessBoard *ChessAlgorithm::board() const
{
    return m_board;
}

double ChessAlgorithm::computeProbability(QList<QString> myStrings, QString stringToCompare)
{
    if (myStrings.isEmpty())
    {
        qDebug()<<"Nothing to compare!";
        return -2;
    }

    int length = stringToCompare.size();
    QList<QString>::iterator it = myStrings.begin();

    while (it!=myStrings.end())
    {
       if ((*it).size()!=length)
       {
           qDebug()<<"Different length of strings!";
           return -1;
       }
       ++it;
    }

    //we use doubles to save accuracy when calculate

    double coeff = 0.0; //coeficient for trained dataset

    QVector<double> myWeights;  //vector of weights for trained dataset
    myWeights.resize(length);
    myWeights.fill(0.0);

    double var = 0.0; //temporary variable for counting a coeficient
    double numberOfSamples = static_cast<double>(myStrings.size());

    double pix = 0.0;    //number of pixels to be taken for trained dataset

    double probability = 0.0;    //probability of string4, result to return

    for(int x = 0; x < length; x++)
    {
       it = myStrings.begin();

       while (it!=myStrings.end())
       {
           if ((*it).at(x) == QChar('P'))
           {
               pix++;
               break;
           }
           ++it;
       }

    }

    for(int x = 0; x < length; x++)
    {
        it = myStrings.begin();

        while (it!=myStrings.end())
        {
            if ((*it).at(x) == QChar('P'))
            {
               myWeights[x] = myWeights[x]+1;
            }
            ++it;
        }

        myWeights[x] = myWeights[x]/numberOfSamples;
        var = var+myWeights[x];
    }

    if (pix == 0 || var == 0)
    {
        return 0;
    }

    coeff = pix/var;

    for(int x = 0; x < length; x++)
    {
        probability = probability+(myWeights[x]*(stringToCompare.at(x) == QChar('P'))*coeff)/pix;
    }

    return probability;
}

void ChessAlgorithm::newGame()
{
    setupBoard();
    board()->setFen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
}

void ChessAlgorithm::setupBoard()
{
    setBoard(new ChessBoard(8,8, this));
}

void ChessAlgorithm::setBoard(ChessBoard *board)
{
    if(board == m_board) return;
    if(m_board) delete m_board;
    m_board = board;
    emit boardChanged(m_board);
}

