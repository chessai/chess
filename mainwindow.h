#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpacerItem>
#include <QButtonGroup>
#include <QRadioButton>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QStatusBar>
#include <QAction>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QPixmap>
#include <QFileDialog>
#include <QInputDialog>



#include "chessview.h"

class ChessAlgorithm;
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void viewClicked(const QPoint &field);

protected:


private slots:
    void on_addPixelButton_clicked();
    void on_removePixelButton_clicked();
    void on_actionSave_pixels_triggered(bool);
    void on_actionDownload_pixels_triggered(bool);
    void on_actionReset_pixels_triggered(bool);
    void on_actionSet_pixels_triggered(bool);
    void on_actionCalculate_triggered(bool);

private:

    ChessView *m_view;
    ChessAlgorithm *m_algorithm;
    QPoint m_clickPoint;
    ChessView::FieldHighlight *m_selectedField;

    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QRadioButton *addPixelButton;
    QRadioButton *removePixelButton;
    QButtonGroup *myButtonGroup;
    QSpacerItem *verticalSpacer;
    ChessView *chessViewWidget;

    QMenuBar *menuBar;
    QMenu *menuMenu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    QAction *actionSave_pixels;
    QAction *actionDownload_pixels;
    QAction *actionReset_pixels;
    QAction *actionSet_pixels;
    QAction *actionCalculate;

    int countFilesSaved;
    QString currentFileOpened;
};

#endif // MAINWINDOW_H
